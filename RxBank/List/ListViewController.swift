//
//  ListViewController.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    static let storyboardName = "List"
    private(set) var viewModel: ListViewModel!
    private var filterButton = UIBarButtonItem()
    
    var onFilterTap: (()->Void)?
    var onRowTap: ((Transaction)->Void)?

    private let bag = DisposeBag()
    
    static func create(with viewModel: ListViewModel) -> ListViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! ListViewController
        vc.viewModel = viewModel
        return vc
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupTableView()
        setupFilter()
        setupErrorMessage()
    }
    
    // MARK: - Setup
    
    private func setupNavigation() {
        title = Constant.List.title
        filterButton = UIBarButtonItem(image: UIImage(), style: .plain, target: nil, action: nil)
        filterButton.onTap = { [weak self] _ in
            self?.onFilterTap?()
        }
        navigationItem.rightBarButtonItem = filterButton
    }
    
    private func setupTableView() {
        // Visual
        tableView.tableFooterView = UIView()
        tableView.rowHeight = Constant.List.rowHeight
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = .white
        
        // Content
        viewModel.filteredTransactions.asObservable().bind(to: tableView.rx.items(cellIdentifier: ListCell.reuseIdentifier, cellType: ListCell.self)) { row, transaction, cell in
            cell.configure(for: transaction)
        }.disposed(by: bag)
        
        // Selection
        tableView.rx.modelSelected(Transaction.self).subscribe(onNext: { [weak self] transaction in
            self?.onRowTap?(transaction)
        }).disposed(by: bag)
        
        // Row deselection
        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            self?.tableView.deselectRow(at: indexPath, animated: true)
        }).disposed(by: bag)
        
        // Reload
        tableView.refreshControl?.rx.controlEvent(.valueChanged).subscribe(onNext: { [weak self] in
            self?.viewModel.fetch()
            self?.tableView.refreshControl?.endRefreshing()
        }).disposed(by: bag)
    }
    
    private func setupFilter() {
        viewModel.isFilterOn.asObservable().subscribe(onNext: { [weak self] isOn in
            self?.setFilter(isOn)
        }).disposed(by: bag)
    }
    
    private func setupErrorMessage() {
        viewModel.onFetchError = { [weak self] in
            self?.showMessage(title: "Can't load transactions", text: "We are unable to load transactions. Please check your connection and try again.")
        }
    }
    
    // MARK: - Toggle filter icon
    
    private func setFilter(_ isOn: Bool) {
        let image = isOn ? Constant.Filter.on : Constant.Filter.off
        filterButton.setBackgroundImage(image, for: .normal, barMetrics: .default)
    }
}
