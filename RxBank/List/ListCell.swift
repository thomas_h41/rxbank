//
//  ListCell.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    static let reuseIdentifier = "listCell"
    
    func configure(for transaction: Transaction) {
        typeImageView.image = transaction.direction.image()
        amountLabel.text = String(transaction.amountInAccountCurrency) + " " + Constant.Account.currency
        typeLabel.text = transaction.direction.firstUppercased()
    }
}
