//
//  ListViewModel.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ListViewModel {
    
    var onFetchError: (()->Void)?
    
    private let transactions = BehaviorSubject(value: [Transaction]())
    let filterModel = Variable([FilterModel(direction: .incoming, isOn: true),
                                FilterModel(direction: .outgoing, isOn: true)])
    var isFilterOn: Observable<Bool> {
        log.debug("filter isOn changed")
        return filterModel.asObservable().map { model in
            let isOnArray = model.map { $0.isOn }
            let result = !isOnArray.reduce(true) { $0 && $1 }
            log.debug("result " + result.description)
            return result
        }
    }
    
    var filteredTransactions: Observable<[Transaction]> {
        return Observable.combineLatest(transactions.asObservable(), isFilterOn)
            .map{ [weak self] transactions, _ in
                guard let `self` = self else { return [] }
                return self.filtered(transactions)
        }
    }
    
    init() {
        fetch()
    }
    
    // MARK: - Fetch transactions
    
    func fetch() {
        Backend.getTransactionList(onSuccess: { [weak self] transactions in
            self?.transactions.onNext(transactions)
        }, onError: { [weak self] text in
            log.error("Error fetching transactions")
            self?.transactions.onNext([])
            self?.onFetchError?()
        })
    }
    
    // MARK: - Filter transactions
    
    private func filtered(_ transactions: [Transaction]) -> [Transaction] {
        return transactions.filter { [weak self] transaction in
            guard let `self` = self else { return false }
            return self.filterModel.value.filter{ $0.isOn }.contains(where: { $0.direction == transaction.direction })
        }
    }
}
