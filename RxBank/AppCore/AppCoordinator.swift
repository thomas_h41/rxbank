//
//  AppCoordinator.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit
import RxSwift

class AppCoordinator {
    
    private let window: UIWindow
    
    var navigationController: UINavigationController = {
        let nc = UINavigationController()
        nc.navigationBar.prefersLargeTitles = true
        nc.setupStyle()
        return nc
    }()
    
    init(window: UIWindow) {
        self.window = window
        self.window.tintColor = Constant.Style.mainStyleColor
    }
    
    func start() {
        presentLaunchScreen(onDidAppear: { [weak self] in
            self?.presentList()
        })
    }
    
    // MARK: - Present
    
    private func presentLaunchScreen(onDidAppear: (() -> Void)? = nil) {
        let launchVC = LaunchViewController.create()
        launchVC.onDidAppear = onDidAppear
        self.window.rootViewController = launchVC
    }
    
    private func presentList() {
        let viewModel = ListViewModel()
        let listVC = ListViewController.create(with: viewModel)
        listVC.onFilterTap = { [weak self] in
            self?.presentFilter(for: viewModel.filterModel)
        }
        listVC.onRowTap = { [weak self] transaction in
            self?.presentDetail(for: transaction)
        }
        navigationController.setViewControllers([listVC], animated: false)
        // Animate switching of the rootViewController
        UIView.transition(with: window, duration: 0.4, options: .transitionCrossDissolve, animations: { [weak self] in
            guard let `self` = self else { return }
            self.window.rootViewController = self.navigationController
        })
    }
    
    private func presentFilter(for model: Variable<[FilterModel]>) {
        let viewModel = FilterViewModel(with: model)
        let filterVC = FilterViewController.create(with: viewModel)
        let nc = UINavigationController()
        nc.setupStyle()
        nc.setViewControllers([filterVC], animated: false)
        nc.modalTransitionStyle = .coverVertical
        nc.modalPresentationStyle = .fullScreen
        nc.navigationBar.prefersLargeTitles = true
        navigationController.present(nc, animated: true)
    }
    
    private func presentDetail(for transaction: Transaction) {
        let viewModel = DetailViewModel(with: transaction)
        let detailVC = DetailViewController.create(with: viewModel)
        navigationController.pushViewController(detailVC, animated: true)
    }
}

