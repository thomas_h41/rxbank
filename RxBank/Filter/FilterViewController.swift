//
//  FilterViewController.swift
//  RxBank
//
//  Created by Tomas Svoboda on 11/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FilterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    static let storyboardName = "Filter"
    private(set) var viewModel: FilterViewModel!
    
    private let bag = DisposeBag()
    
    static func create(with viewModel: FilterViewModel) -> FilterViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! FilterViewController
        vc.viewModel = viewModel
        return vc
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupTableView()
    }
    
    // MARK: - Setup
    
    private func setupNavigation() {
        title = Constant.Filter.title
        // Done button
        let doneButton = UIBarButtonItem(title: LocalizedString.done, style: .done, target: nil, action: nil)
        doneButton.onTap = { [weak self] _ in
            self?.dismiss(animated: true)
        }
        navigationItem.rightBarButtonItem = doneButton
    }
    
    private func setupTableView() {
        // Visual
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = Constant.Filter.estimatedRowHeight
        
        // Content
        viewModel.rows.asObservable().bind(to: tableView.rx.items(cellIdentifier: FilterCell.reuseIdentifier, cellType: FilterCell.self)) { index, row, cell in
            switch row {
            case .item(let model, _): cell.configure(for: model)
            case .sectionHeader(let title): cell.configureHeader(for: title)
            case .selectAll(let title, _), .clearAll(let title, _): cell.configureButton(for: title)
            }
            }.disposed(by: bag)
        
        // Selection
        tableView.rx.modelSelected(FilterRow.self).subscribe(onNext: { row in
            switch row {
            case .item(let model, let onSelected): onSelected(model)
            case .sectionHeader: break
            case .selectAll(_, let onSelected), .clearAll(_, let onSelected): onSelected()
            }
        }).disposed(by: bag)
    }
}
