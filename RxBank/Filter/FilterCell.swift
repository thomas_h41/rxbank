//
//  FilterCell.swift
//  RxBank
//
//  Created by Tomas Svoboda on 11/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectImageView: UIImageView!
    
    static let reuseIdentifier = "filterCell"
    
    /* The reason for reusing a single cell custom type for 3 different types of cells is the use of simple RxSwift tableView
     binding. Multiple custom cell types require use of RxSwiftDataSources framework which is slightly harder to understand,
     hence I have used the simple binding and configure cells for different layout, as I don't know what is the level of
     RxSwift knowledge in the rest of the team. */
    
    func configure(for model: FilterModel) {
        iconImageView.image = model.direction.image()
        titleLabel.text = model.direction.firstUppercased()
        selectImageView.image = model.isOn ? #imageLiteral(resourceName: "selected") : #imageLiteral(resourceName: "unselected")
        setImages(isHidden: false)
        titleLabel.setStyle(for: .item)
    }
    
    func configureHeader(for title: String) {
        titleLabel.text = title
        setImages(isHidden: true)
        titleLabel.setStyle(for: .header)
    }
    
    func configureButton(for title: String) {
        titleLabel.text = title
        setImages(isHidden: true)
        titleLabel.setStyle(for: .button)
    }
    
    // MARK: - Private implementations
    
    private func setImages(isHidden: Bool) {
        iconImageView.isHidden = isHidden
        selectImageView.isHidden = isHidden
    }
}

enum RowStyle {
    case item, header, button
}

extension UILabel {
    
    func setStyle(for style: RowStyle) {
        switch style {
        case .item:
            self.textColor = .black
            self.font = UIFont.systemFont(ofSize: Constant.Filter.itemFontSize, weight: .regular)
        case .header:
            self.textColor = .black
            self.font = UIFont.systemFont(ofSize: Constant.Filter.headerFontSize, weight: .semibold)
        case .button:
            self.textColor = Constant.Style.mainStyleColor
            self.font = UIFont.systemFont(ofSize: Constant.Filter.itemFontSize, weight: .regular)
        }
    }
}
