//
//  FilterViewModel.swift
//  RxBank
//
//  Created by Tomas Svoboda on 11/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit
import RxSwift

class FilterModel {
    let direction: Transaction.Direction
    var isOn: Bool
    
    init(direction: Transaction.Direction, isOn: Bool) {
        self.direction = direction
        self.isOn = isOn
    }
}

enum FilterRow {
    case sectionHeader(String)
    case item(FilterModel, (FilterModel)->Void)
    case selectAll(String, ()->Void)
    case clearAll(String, ()->Void)
}

class FilterViewModel {
    
    let filterModel: Variable<[FilterModel]>
    var rows = Variable([FilterRow]())
    var onSelected: ((FilterModel)->Void)!
    
    init(with model: Variable<[FilterModel]>) {
        self.filterModel = model
        onSelected = { [weak self] model in
            self?.toggleSelection(for: model)
        }
        // Map filterModel into rows
        rows.value = filterModel.value.map{ filterModel in
            FilterRow.item(filterModel, onSelected)
        }
        // Add section header row
        rows.value.insert(FilterRow.sectionHeader(Constant.Filter.sectionHeader), at: 0)
        // Add select/deselect all rows
        rows.value.append(FilterRow.selectAll(Constant.Filter.selectAll, { [weak self] in self?.selectAll() }))
        rows.value.append(FilterRow.clearAll(Constant.Filter.clearAll, { [weak self] in self?.clearAll() }))
    }
    
    // MARK: - Private implementation
    
    private func toggleSelection(for model: FilterModel) {
        model.isOn = !model.isOn
        update(model, with: onSelected)
    }
    
    private func selectAll() {
        setAll(true)
    }
    
    private func clearAll() {
        setAll(false)
    }
    
    private func setAll(_ isOn: Bool) {
        filterModel.value.forEach {
            $0.isOn = isOn
        }
        reloadFilterModel()
    }
    
    private func reloadFilterModel() {
        for case let .item(model, onSelected) in rows.value {
            update(model, with: onSelected)
        }
    }
    
    private func update(_ model: FilterModel, with onSelected: @escaping (FilterModel)->Void) {
        guard let index = index(for: model) else { return }
        rows.value[index + 1] = FilterRow.item(model, onSelected) // Section header is at index 0
        filterModel.value = filterModel.value // Stupid way of emitting event
    }
    
    private func index(for model: FilterModel) -> Int? {
        return filterModel.value.index(where: { $0.direction == model.direction})
    }
}
