//
//  Log.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation
import SwiftyBeaver

let log = SwiftyBeaver.self

class Log {
    
    static func configure() {
        let console = ConsoleDestination()
        console.format = "$DHH:mm:ss.SSS$d $C$L$c $C$c$M"
        log.addDestination(console)
    }
}
