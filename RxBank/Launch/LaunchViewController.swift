//
//  LaunchViewController.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    static let storyboardName = "Launch"
    var onDidAppear: (() -> Void)?
    
    static func create() -> LaunchViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! LaunchViewController
        return vc
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Prepared for additional visual changes after the application launches
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0)) { [weak self] in
            self?.onDidAppear?()
        }
    }
}
