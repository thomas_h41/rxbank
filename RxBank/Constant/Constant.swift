//
//  Constant.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

struct Constant {
    
    struct Style {
        static let mainStyleColor = #colorLiteral(red: 0.5529411765, green: 0.768627451, blue: 0.2823529412, alpha: 1)
    }
    
    struct Account {
        static let currency = "Kč"
        static let name = "Account #1"
    }
    
    struct List {
        static let title = Account.name
        static let rowHeight: CGFloat = 60.0
    }
    
    struct Filter {
        static let off = #imageLiteral(resourceName: "filterOff")
        static let on = #imageLiteral(resourceName: "filterOn")
        static let title = "Filter"
        static let sectionHeader = "Direction"
        static let selectAll = "Select all"
        static let clearAll = "Clear all"
        static let estimatedRowHeight: CGFloat = 44.0
        static let itemFontSize: CGFloat = 17.0
        static let headerFontSize: CGFloat = 22.0
    }
    
    struct Detail {
        static let title = "Detail"
        static let accountNumberTitle = "Account Number"
        static let accountNameTitle = "Account Name"
        static let bankCodeTitle = "Bank Code"
    }
}

struct LocalizedString {
    
    static let yes = LS("common.yes", value: "Yes", comment: "Yes")
    static let ok = LS("common.ok", value: "OK", comment: "OK")
    static let cancel = LS("common.cancel", value: "Cancel", comment: "Cancel")
    static let done = LS("common.done", value: "Done", comment: "Done")
    static let selectAll = LS("common.selectAll", value: "Select All", comment: "Select all")
    static let delete = LS("common.delete", value: "Delete", comment: "Common delete")
    static let confirm = LS("common.confirm", value: "Confirm", comment: "Accept some action")
    static let loading = LS("common.loading", value: "Loading...", comment: "Loading")
    
    static let errorOccured = LS("common.errorOccured", value: "An error occured", comment: "An error occured")
    static let knownErrorOccured = LS("common.knownErrorOccured", value: "Error occured", comment: "The error occured")
    static let optional = LS("common.placeholder.optional", value: "Optional", comment: "Placeholder of a text field which indicates that the fill in of value is optional.")
    static let required = LS("common.placeholder.required", value: "Required", comment: "Placeholdr of a text field which indicates that the fill in of value is required.")
    static let edit = LS("common.edit", value: "Edit", comment: "Common edit")
}
