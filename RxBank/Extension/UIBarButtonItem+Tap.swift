//
//  UIBarButtonItem+Tap.swift
//  RxBank
//
//  Created by Tomas Svoboda on 11/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

typealias BarButtonAction = (UIBarButtonItem) -> Void

extension UIBarButtonItem {
    
    private struct AssociatedKeys {
        static var ActionKey = "ActionKey"
    }
    
    private class ActionWrapper {
        let action: BarButtonAction
        init(action: @escaping BarButtonAction) {
            self.action = action
        }
    }
    
    convenience init(image: UIImage, style: UIBarButtonItemStyle, block: BarButtonAction?) {
        self.init(image: image, style: style, target: nil, action: nil)
        self.onTap = block
    }
    
    convenience init(barButtonSystemItem systemItem: UIBarButtonSystemItem, block: BarButtonAction?) {
        self.init(barButtonSystemItem: systemItem, target: nil, action: nil)
        self.onTap = block
    }
    
    var onTap: BarButtonAction? {
        set(newValue) {
            action = nil
            target = nil
            var wrapper: ActionWrapper? = nil
            if let newValue = newValue {
                wrapper = ActionWrapper(action: newValue)
                target = self
                action = #selector(performAction)
            }
            
            objc_setAssociatedObject(self, &AssociatedKeys.ActionKey, wrapper, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            guard let wrapper = objc_getAssociatedObject(self, &AssociatedKeys.ActionKey) as? ActionWrapper else {
                return nil
            }
            
            return wrapper.action
        }
    }
    
    @objc func performAction(_ sender: UIBarButtonItem) {
        onTap?(sender)
    }
}
