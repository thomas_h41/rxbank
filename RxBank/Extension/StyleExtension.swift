//
//  StyleExtension.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setupStyle() {
        navigationBar.isOpaque = true
        navigationBar.isTranslucent = false
        navigationBar.barStyle = .blackOpaque
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = Constant.Style.mainStyleColor
        navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        view.backgroundColor = UIColor.white
    }
}

extension UISearchController {
    
    func setupStyle() {
        searchBar.backgroundColor = Constant.Style.mainStyleColor
        dimsBackgroundDuringPresentation = false
        hidesNavigationBarDuringPresentation = false
        searchBar.tintColor = UIColor.white
    }
}

extension UIAlertController {
    
    func setupStyle() {
        view.tintColor = Constant.Style.mainStyleColor
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
