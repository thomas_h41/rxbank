//
//  UIViewController+Alert.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showMessage(title: String?, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: LocalizedString.ok, style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func presentAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alert.addAction($0) }
        alert.setupStyle()
        present(alert, animated: true, completion: nil)
    }
    
    func showConfirmationAlert(title: String?, message: String?, onConfirmed: @escaping () -> Void, onCancel: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmStr = LocalizedString.yes
        let cancelStr = LocalizedString.cancel
        alert.addAction(UIAlertAction(title: confirmStr, style: .default, handler: { _ in onConfirmed() }))
        alert.addAction(UIAlertAction(title: cancelStr, style: .cancel, handler: { _ in onCancel?() }))
        alert.setupStyle()
        present(alert, animated: true, completion: nil)
    }
    
    @discardableResult func showMessage(title: String, message: String, confirmAction: AlertChoice, cancelAction: AlertChoice? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirm = UIAlertAction(title: confirmAction.title, style: confirmAction.style, handler: confirmAction.action)
        alert.addAction(confirm)
        if let cancelAction = cancelAction {
            let cancel = UIAlertAction(title: cancelAction.title, style: cancelAction.style, handler: cancelAction.action)
            alert.addAction(cancel)
        }
        alert.setupStyle()
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    @discardableResult func alertWithTextField(title: String, message: String, confirmTitle: String, confirmAction: @escaping (_ returnValue: String?) -> () , cancelAction: AlertChoice? = nil, textFieldHander: ((UITextField) -> Void)? = nil ) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirm = UIAlertAction(title: confirmTitle, style: .default) { (_) in
            let textFieldOutput = alert.textFields?.first?.text
            confirmAction(textFieldOutput)
        }
        
        alert.addAction(confirm)
        if let cancelAction = cancelAction {
            let cancel = UIAlertAction(title: cancelAction.title, style: cancelAction.style, handler: cancelAction.action)
            alert.addAction(cancel)
        }
        alert.addTextField { (textField) in
            textFieldHander?(textField)
        }
        alert.setupStyle()
        present(alert, animated: true, completion: nil)
        
        return alert
    }
}


// MARK: - Multi-choice alert model

struct AlertChoice {
    let title: String
    let action: AlertAction?
    let style: UIAlertActionStyle
    public init(title: String, action: AlertAction?, style: UIAlertActionStyle) {
        self.title = title
        self.action = action
        self.style = style
    }
}

public typealias AlertAction = (UIAlertAction) -> Void
