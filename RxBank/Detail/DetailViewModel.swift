//
//  DetailViewModel.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation

class DetailViewModel {
    
    var transaction: Transaction
    var onFetchSuccess: (()->Void)?
    var onFetchError: (()->Void)?

    init(with transaction: Transaction) {
        self.transaction = transaction
        fetch(transaction.id)
    }
    
    // MARK: - Fetch transaction detail
    
    private func fetch(_ id: Int) {
        Backend.getTransactionDetail(for: id, onSuccess: { [weak self] contraAccount in
            self?.transaction.contraAccount = contraAccount
            self?.onFetchSuccess?()
            }, onError: { [weak self] text in
                log.error("Error fetching transaction detail")
                self?.onFetchError?()
        })
    }
}
