//
//  DetailViewController.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DetailViewController: UIViewController {

    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var accountNumber: UILabel!
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var bankCode: UILabel!
    
    @IBOutlet weak var accountNumberValue: UILabel!
    @IBOutlet weak var accountNameValue: UILabel!
    @IBOutlet weak var bankCodeValue: UILabel!
    
    static let storyboardName = "Detail"
    private(set) var viewModel: DetailViewModel!

    static func create(with viewModel: DetailViewModel) -> DetailViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! DetailViewController
        vc.viewModel = viewModel
        return vc
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupTransaction()
        setupContraAccount()
        viewModel.onFetchSuccess = { [weak self] in
            self?.fillContraAccountValues()
        }
        setupErrorMessage()
    }
    
    // MARL: - Setup
    
    private func setupNavigation() {
        title = Constant.Detail.title
    }
    
    private func setupTransaction() {
        typeImageView.image = viewModel.transaction.direction.image()
        amountLabel.text = String(viewModel.transaction.amountInAccountCurrency) + " " + Constant.Account.currency
        typeLabel.text = viewModel.transaction.direction.firstUppercased()
    }
    
    private func setupContraAccount() {
        accountNumber.text = Constant.Detail.accountNumberTitle
        accountName.text = Constant.Detail.accountNameTitle
        bankCode.text = Constant.Detail.bankCodeTitle
        accountNumberValue.text = ""
        accountNameValue.text = ""
        bankCodeValue.text = ""
    }
    
    private func fillContraAccountValues() {
        accountNumberValue.text = viewModel.transaction.contraAccount?.accountNumber
        accountNameValue.text = viewModel.transaction.contraAccount?.accountName
        bankCodeValue.text = viewModel.transaction.contraAccount?.bankCode
    }
    
    private func setupErrorMessage() {
        viewModel.onFetchError = { [weak self] in
            self?.showMessage(title: "Can't load transaction detail", text: "We are unable to load transaction detail. Please check your connection and try again.")
        }
    }
}
