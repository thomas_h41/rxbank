//
//  URLBuilder.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation

// Constants
private let slash = "/"
private let protocolSufix = "://"
private let http = "http"
private let https = "https"
// Endpoints
private let transactionsEndpoint = "transactions"

enum Endpoint {
    case transactions, detail
}

enum NetProtocol: String {
    case http = "http"
    case https = "https"
}

enum Environment: String {
    case demo = "demo0569565.mockable.io"
    case dev = "dev.rxbank.io"
    case prod = "prod.rxbank.io"
}

struct URLBuilder {
    
    // MARK: - Public interface
    
    static func url(for endpoint: Endpoint, in environment: Environment, for id: Int? = nil) -> String? {
        switch endpoint {
        case .transactions: return transaction(in: environment)
        case .detail: return detail(in: environment, for: id)
        }
    }
    
    // MARK: - Private implementation
    
    // Endpoint URL
    
    private static func transaction(in environment: Environment) -> String {
        return URLBuilder.netProcool + environment.rawValue + URLBuilder.transactionsPath
    }
    
    private static func detail(in environment: Environment, for id: Int?) -> String? {
        guard let id = id else { log.error("Missing transaction id in URLBuilder"); return nil }
        return URLBuilder.netProcool + environment.rawValue + URLBuilder.transactionsPath + slash + String(id)
    }
    
    // Protocol
    
    private static var netProcool: String {
        return NetProtocol.http.rawValue + protocolSufix
    }
    
    // Paths
    
    private static var transactionsPath: String {
        return slash + transactionsEndpoint
    }
}
