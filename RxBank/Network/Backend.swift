//
//  Backend.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation
import Alamofire

private let queue = DispatchQueue.global(qos: .userInitiated)

class Backend {
    
    // MARK: - Get transactions
    
    static func getTransactionList(onSuccess: @escaping (_ transactions: [Transaction]) -> Void, onError: @escaping (String?)->Void) {
        
        guard let url = URLBuilder.url(for: .transactions, in: .demo) else {
            assertionFailure("Incorrectly built URL for transactions")
            onError(nil)
            return
        }
        let params = [String: String]()
        
        Alamofire.request(url, parameters: params)
            .validate().responseString(queue: queue) { response in
                
                Backend.process(response, with: onError) { jsonString in
                    guard let jsonString = jsonString else {
                        assertionFailure("Can't cast json as string")
                        onError(nil)
                        return
                    }
                    guard let transactionResponse = TransactionResponse.from(json: jsonString) else {
                        assertionFailure("Can't parse transaction list")
                        onError(nil)
                        return
                    }
                    onSuccess(transactionResponse.transactions)
                }
        }
    }
    
    // MARK: - Get transaction detail
    
    static func getTransactionDetail(for id: Int, onSuccess: @escaping (_ contraAccount: ContraAccount) -> Void, onError: @escaping (String?)->Void) {
        
        guard let url = URLBuilder.url(for: .detail, in: .demo, for: id) else {
            assertionFailure("Incorrectly built URL for transaction detail")
            onError(nil)
            return
        }
        let params = [String: String]()
        
        Alamofire.request(url, parameters: params)
            .validate().responseString(queue: queue) { response in
                
                Backend.process(response, with: onError) { jsonString in
                    guard let jsonString = jsonString else {
                        assertionFailure("Can't cast json as string")
                        onError(nil)
                        return
                    }
                    guard let detailResponse = DetailResponse.from(json: jsonString) else {
                        assertionFailure("Can't parse transaction list")
                        onError(nil)
                        return
                    }
                    onSuccess(detailResponse.contraAccount)
                }
        }
    }
    
    // MARK: - Process response with networkerror handlign
    
    static func process(_ response: DataResponse<String>, with onError: @escaping (String?)->Void, onSuccess: @escaping (_ jsonString: String?) -> Void) {
        
        DispatchQueue.main.async {
            switch (response.result, response.response?.statusCode) {
            case (.success, _):
                onSuccess(response.result.value)
            case (.failure, .some(401)):
                onError(nil)
                log.error("401 Not authorized")
            case (.failure, .some(500)):
                onError(response.data.flatMap{ String(data: $0, encoding: .utf8)} ?? nil)
                log.error("500 Internal Server Error")
            case (.failure, _):
                onError(nil)
                log.error("\(response.response?.statusCode ?? 0) Network Error")
            }
        }
    }
}
