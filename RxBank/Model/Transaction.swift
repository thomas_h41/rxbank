//
//  Transaction.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import UIKit

struct TransactionResponse: Decodable {
    let transactions: [Transaction]
    
    enum CodingKeys: String, CodingKey {
        case transactions = "items"
    }
}

struct Transaction {
    let id: Int
    let amountInAccountCurrency: Int
    let direction: Direction
    var contraAccount: ContraAccount?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case amountInAccountCurrency = "amountInAccountCurrency"
        case direction = "direction"
    }
    
    enum Direction: String {
        case incoming = "INCOMING"
        case outgoing = "OUTGOING"
        
        func firstUppercased() -> String {
            return self.rawValue.lowercased().capitalizingFirstLetter()
        }
        
        func image() -> UIImage {
            switch self {
            case .incoming: return #imageLiteral(resourceName: "incoming")
            case .outgoing: return #imageLiteral(resourceName: "outgoing")
            }
        }
    }
    
    enum ParsingError: Error {
        case unknownDirection
    }
}

extension Transaction: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        amountInAccountCurrency = try values.decode(Int.self, forKey: .amountInAccountCurrency)
        contraAccount = nil
        
        // Direction
        let directionValue = try values.decode(String.self, forKey: .direction)
        guard let direction = Transaction.Direction(rawValue: directionValue) else {
            throw Transaction.ParsingError.unknownDirection
        }
        self.direction = direction
    }
}

// Serialization extensions

extension TransactionResponse {
    static func from(json: String, using encoding: String.Encoding = .utf8) -> TransactionResponse? {
        guard let data = json.data(using: encoding) else { return nil }
        return TransactionResponse.from(data: data)
    }
    
    static func from(data: Data) -> TransactionResponse? {
        let decoder = JSONDecoder()
        return try? decoder.decode(TransactionResponse.self, from: data)
    }
}
