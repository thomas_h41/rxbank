//
//  TransactionDetail.swift
//  RxBank
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import Foundation

struct DetailResponse: Codable {
    let contraAccount: ContraAccount
}

struct ContraAccount: Codable {
    let accountNumber: String
    let accountName: String
    let bankCode: String
}

// Serialization extensions

extension DetailResponse {
    static func from(json: String, using encoding: String.Encoding = .utf8) -> DetailResponse? {
        guard let data = json.data(using: encoding) else { return nil }
        return DetailResponse.from(data: data)
    }
    
    static func from(data: Data) -> DetailResponse? {
        let decoder = JSONDecoder()
        return try? decoder.decode(DetailResponse.self, from: data)
    }
    
    var jsonData: Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(self)
    }
    
    var jsonString: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
