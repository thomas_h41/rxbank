//
//  RxBankTests.swift
//  RxBankTests
//
//  Created by Tomas Svoboda on 07/09/2018.
//  Copyright © 2018 hatchery41. All rights reserved.
//

import XCTest
@testable import RxBank

private let urlError = "Incorrectly built URL"

class URLBuilderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testListDemoURL() {
        let expected = "http://demo0569565.mockable.io/transactions"
        let url = URLBuilder.url(for: .transaction, in: .demo)
        XCTAssert(url == expected, urlError)
    }
    
    func testListProdURL() {
        let expected = "http://prod.rxbank.io/transactions"
        let url = URLBuilder.url(for: .transaction, in: .prod)
        XCTAssert(url == expected, urlError)
    }
    
    func testDetailDevURL() {
        let expected = "http://dev.rxbank.io/transactions/2"
        let url = URLBuilder.url(for: .detail, in: .dev, id: 2)
        XCTAssert(url == expected, urlError)
    }
    
    func testDetailURLError() {
        let expected: String? = nil
        let url = URLBuilder.url(for: .detail, in: .demo)
        XCTAssert(url == expected, urlError)
    }
}
